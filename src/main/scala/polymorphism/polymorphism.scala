package polymorphism
object Polymorphism extends App {

  //Higher class
  trait Animal {

    def make_sound
  }
  //Cat overrides make_sound
  class Cat() extends Animal {

    def make_sound = {

      println("Meow")

    }
  }
  //Dog overrides make_sound with different value
  class Dog() extends Animal {

    override def make_sound = {

      println("Woof")

    }
  }

  val cat = new Cat()

  val dog = new Dog()

  val animals = List(cat, dog)

  for (each <- animals) {
    each.make_sound
  }

}